#!/bin/bash

# PARTE 1: PREPROCESADO DE LAS IMAGENES PARA LLEVAR
# 	A CABO EL PROCESO DEL PIPELINE
if [[ $1 = "-1" || $1 = "-all" ]]; then
	scripts/data_prepare.sh
fi

# PARTE 2: PIPELINE COMPLETO DE FSL
if [[ $1 = "-2" || $1 = "-all" ]]; then
	#reorient images so they fit the template
	mkdir ReorientedScreening
	scripts/reorient.pl PD_positives
	scripts/reorient.pl PD_control

	#affine normalization for the images
	mkdir MNI152AffineNormalizedScreening
	scripts/AffineMNI152NormalizationFSL.pl

	#bias correction
	#necessary to unzip before using ANTs
	mkdir MNI152AffineNormalizedN3
	scripts/unzip_images.sh
	scripts/N3BiasFieldCorrection.pl

	#skull stripping
	mkdir MNI152AffineNormalizedN3NoSkull
	scripts/SkullStripping.pl

	#Label the data in positives and control
	> labels.txt
	cd MNI152AffineNormalizedN3NoSkull
	../scripts/data_labelling.sh
	cd ..
fi

# PARTE 3: HERRAMIENTAS FAST Y FIRST 
# PARA OBTENER GM,WM,CSF y FEATURES DE TODAS
# LAS ROIS(15)
if [[ $1 = "-3" || $1 = "-all" ]]; then
	mkdir fastResults
	scripts/fast_quantification.pl
	> CSF_results.txt
	> GM_results.txt
	> WM_results.txt
	scripts/fast_results.sh

	mkdir firstResults
	scripts/first_application.pl
	mkdir files
	scripts/file_creation.sh
	scripts/rois_quantification.sh	

fi
