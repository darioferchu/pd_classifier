##Code used to preprocess cerebral MRI images of PD and Control patients
and then create a classification model in order to predict class belonging
of new data.
- /scripts contains all the scripts used in preprocess stage. This are shell
and perl scripts that use ANTs, FSL, and vtk software.
- /files contains data extracted from different ROIs of the brain.
- CSF,GM,WM files have the segmentation data from Cerebral fluid, Grey matter and
    White matter, also used when doing the classification.
- pipeline.sh is the shell script that carries the processing pipeline.
-classification.m Matlab code to do the classification and evaluate it.