% Dario Ferrer Chueca
% Fichero classification.m
% Fichero para trabajar con las imagenes MRI
% obtenidas tras el pipeline de preprocesamiento

%Package to load the functions for handling nii images
addpath('../NIfTI_20140122'); 

%Folder where final images are stored
addpath('MNI152AffineNormalizedN3NoSkull');

%Folder that contains all images, with the new ones added
addpath('MNI152AffineNormalizedN3NoSkullCombined');

%Folder where ROI data is stored
addpath('files');
addpath('filesCombined');

%Control variables
combined=0;  %tomar todos los datos
init=0;
voxels=0;
remove_zeros=1;
normalize_intensities=0;
score_default=0;
pca_application=0;
t_test_features=0;
classification_standardize=true;
validation=1;
roi=1;
training=0;
testing=1;
plotting=0;
combined_plot=0;
voxel_pattern_distribution=0;
%Number of voxels extracted in t-test(decreasing order of t-score)
num_features_voxels=213;


% Initialize data, get files that contain nii images and
% rest of features extracted from brains. Also the class of
% every subject
if init
    if combined
        [files,roi_files]=load_files(combined);
        CSF_data = importdata('CSF_results_combined.txt');
        GM_data = importdata('GM_results_combined.txt');
        WM_data = importdata('WM_results_combined.txt');
        y=importdata('labelsCombined.txt');
    else
        [files,roi_files]=load_files(combined);
        CSF_data = importdata('CSF_results.txt');
        GM_data = importdata('GM_results.txt');
        WM_data = importdata('WM_results.txt');
        y=importdata('labels.txt');
    end
end

% Load original feature matrix. Contains all voxels
if voxels
    X=load_voxels(files,remove_zeros,normalize_intensities);
end

% PCA to reduce dimension. Data is centered by default
if pca_application
    [coeff,score,latent,tsquared,explained] = pca(X,'Centered',false);
end

% T-test method to reduce number of features
if t_test_features
    [score,t_order]=t_test(X,y,num_features_voxels);
end

% In case neither PCA nor T-test are applied
if score_default
    score=X;
end


% Validation of the classification. Both SVM and KNN methods are tested
% with different kernels and distance metrics respectively.
if validation
    if training
        other_features=[];
        other_features = [other_features CSF_data GM_data WM_data];
        other_features=add_roi_data(roi_files,other_features);

        %separate data for train and test
        %train data is after divided into train and test for CV
        %best model is calculated with cross validation and then used in test
        %data

        %try to get equal amount of positives and negatives, 20% of data
        equal=false;
        while not(equal) 
            cv = cvpartition(size(score,1),'HoldOut',0.2);
            test_idx = cv.test;
            %test_idx=randperm(size(score,1),round(size(score,1)*20/100));
            y_test_after=y(test_idx,:);
            n=groupcounts(y_test_after);
            if n(1)>7 && n(2)>7
                test_set_after=score(test_idx,:);
                test_other_features=other_features(test_idx,:);
                equal=true;
            end
        end    



        score=score(test_idx ~= 1, :);
        other_features=other_features(test_idx ~= 1, :);
        y=y(test_idx ~= 1, :);

        [resultados_svm,models_svm,indexes_train]=svmtraining(score,other_features,y,roi,classification_standardize);
        [resultados_knn,resultados_k,models_knn]=knntraining(score,other_features,y,roi,classification_standardize);

    end
    if testing
        %test with the best models to check their precision
        %use amount of voxels used in model and features from ROIs.


        [best_model_SVM,kernel_type,num_features_SVM,best_model_KNN,distance_metric,num_features_KNN]=get_best_model(resultados_svm,models_svm,resultados_knn,models_knn);

        %Get all features
        test_set_after_SVM=test_set_after(:,1:num_features_SVM+9);
        final_test_set_SVM=[test_set_after_SVM test_other_features];

        test_set_after_KNN=test_set_after(:,1:num_features_KNN+9);
        final_test_set_KNN=[test_set_after_KNN test_other_features];
        
        %standardize with mean and std from the model(NOT NECESSARY)
        %final_test_set=(test_set_after-best_model_SVM.Mu)./best_model_SVM.Sigma;
        %final_test_set=final_test_set./best_model_SVM.KernelParameters.Scale;
        [precisions_SVM_test,precisions_KNN_test]=test_performance(best_model_SVM,best_model_KNN,final_test_set_SVM,final_test_set_KNN,y_test_after);
    end
end    

if plotting
    plot_svm_definitive(resultados_svm,roi,size(score,2));
    plot_knn_definitive(resultados_knn,resultados_k,roi,size(score,2));
    [svm_precisions,knn_precisions,k_values]=get_best_results(resultados_svm,resultados_knn,resultados_k);
end    

if combined_plot
    plot_svm_pca_ttest(roi,213);
    %plot_knn_pca_ttest(roi,213,0);
end

if voxel_pattern_distribution
    [best_model_SVM,best_model_KNN]=get_best_model(resultados_svm,models_svm,resultados_knn,models_knn);
    [weights_SVM,weights_KNN]=get_weights(best_model_SVM, best_model_KNN);
    %once we got the weights, depends if it is pca or t-test
    X_full=load_voxels(files,0,0);
    if pca_application
       %back project and multiply by weights 
    else
       %t-test case
       %multiply by weights
       final_image=obtain_sum(X_full,weights_SVM);
       
    end    
end  



% -----------------------------
% -----------------------------
% -----------------------------
%   Added functions
% -----------------------------
% -----------------------------
% -----------------------------


function files = point_deletion(files)
    point={'.'};
    mask=[];
    for i=1:3
        if strncmpi(files(i).name,point,1)
            mask=[mask i];
        end
    end
    files(mask)=[];
end

function [files,roi_files] = load_files(combined)
    %point={'.'};   
    if combined
        files = dir('MNI152AffineNormalizedN3NoSkullCombined');
        roi_files = dir('filesCombined');
        files=point_deletion(files);
        roi_files=point_deletion(roi_files);
    else
        files = dir('MNI152AffineNormalizedN3NoSkull');
        roi_files = dir('files');
        files=point_deletion(files);
        roi_files=point_deletion(roi_files);
    end
end

function X =load_voxels(files,remove_zeros,normalize_intensities)
    X=[];
    for i=1:length(files)
      files(i).name
      img=load_nii(files(i).name);
      X(i,:)=reshape(img.img,1,[]); 
    end
    if remove_zeros
        X( :, all(~X,1) ) = [];
    end
    if normalize_intensities
       max_value = max(X, [], 'all');
       X=X/max_value; 
    end
end
function score = add_roi_data(roi_files,score)
    %add roi data
    %length(roi_files)
    for i=1:45    
        roi_data=importdata(roi_files(i).name);
        score=[score roi_data];
    end
end

function [selected_features,t_order] = t_test(X,y,numero_features)
    %para cada una de las features, mascara para las dos clases
    %1-> control, 2-> PD
    %X = randi([1 10],75,10);
    n=groupcounts(y);
    n1=n(1);
    n2=n(2);
    mask_1=y==1;
    mask_2=y==2;
    subjects_1=X(mask_1,:);

    subjects_2=X(mask_2,:);
    
    means_1=mean(subjects_1);
    means_2=mean(subjects_2);
    sd_1=std(subjects_1);
    sd_2=std(subjects_2);
    t=(abs(means_1-means_2))./sqrt((sd_1).^2/n1+(sd_2).^2/n2);
    [t_sorted, t_order] = sort(t,'descend');
    %coeficientes=t_sorted(:,1:1000);
    new_order_X = X(:,t_order);
    selected_features=new_order_X(:,1:numero_features);
end

function [precisions_SVM,precisions_KNN]=test_performance(SVM_models,KNN_models,Xtest_SVM,Xtest_KNN,y_test)
    %check how does the model perform with new data
    precisions_SVM=[];
    precisions_KNN=[];
    for i=1:length(SVM_models)
        [y_predicted, scores_] = predict(SVM_models,Xtest_SVM);
        classifier_precision=(length(y_test)-numel(find(y_predicted~=y_test)))/length(y_test);
        precisions_SVM=[precisions_SVM classifier_precision];
    end
    for i=1:length(KNN_models)
        [y_predicted, scores_] = predict(KNN_models,Xtest_KNN);
        classifier_precision=(length(y_test)-numel(find(y_predicted~=y_test)))/length(y_test);
        precisions_KNN=[precisions_KNN classifier_precision];
    end
end


%Para evaluar los distintos metodos de clasificacion SVM teniendo en cuenta
%lineal, cuadratico,cubico,gaussiano. Se utiliza 10-cross-validation para
%obtener el error medio para las clasificaciones.
function [results_svm,results_svm_models,all_indexes_used]=svmtraining(score,other_features,y,additional_features,classification_standardize)
    num_folds=10;
    results_svm=[];
    results_svm_models=[];
    all_indexes_used=[];
    methods=["linear","2","3","gaussian"];
    for i=1:length(methods)
        method_results=[];
        model_results={};
        method_indexes=[];
        accum_errors=0;
        for numFeatures=10:size(score,2)
            accum_precision=0;
            score_limited=score(:,1:numFeatures);
            if additional_features
                score_limited=[score_limited other_features];
            end
            cv = cvpartition(y,'KFold',num_folds);
            best_fold_precision=0.0;
            best_model=[];
            best_index=[];
            for f=1:num_folds
                idx = training(cv,f); % get partition ith
                %testIdx = (idx ~= i); % index of test subjects
                test_fold = score_limited(idx ~= 1, :);
                train_fold = score_limited(idx == 1, :);
                y_training = y(idx == 1, :);
                y_test=y(idx ~= 1, :);
                if methods(i)~= 'linear' && methods(i)~= 'gaussian'
                    model=fitcsvm(train_fold,y_training,'KernelFunction','polynomial','PolynomialOrder',str2num(methods(i)),'KernelScale','auto','Standardize',classification_standardize);
                else
                    model=fitcsvm(train_fold,y_training,'KernelFunction',methods(i),'KernelScale','auto','Standardize',classification_standardize);
                end
                [y_predicted, scores_] = predict(model,test_fold);
                classifier_precision=(length(y_test)-numel(find(y_predicted~=y_test)))/length(y_test);
                if classifier_precision>best_fold_precision
                   best_model=model;
                   best_fold_precision=classifier_precision;
                   best_index=idx;
                end    
                accum_precision=accum_precision+classifier_precision;
            end
            method_results=[method_results accum_precision/num_folds];
            model_results{numFeatures-9}=best_model;
            method_indexes=[method_indexes best_index];
        end
        results_svm=[results_svm;method_results];
        results_svm_models=[results_svm_models;model_results];
        all_indexes_used=[all_indexes_used method_indexes];
    end
    results_svm_previous=results_svm;
end


function [results_knn,results_k,results_knn_models]=knntraining(score,other_features,y,additional_features,classification_standardize)
    num_folds=10;
    distance_metric=["chebychev","correlation","cosine","euclidean","mahalanobis","minkowski"];    
    results_knn=[];
    results_k=[];
    results_knn_models=[];
    for i=1:length(distance_metric)
        featurenumber_results=[];
        best_Ks=[]
        best_models_feature={};
        for numFeatures=10:size(score,2)
            accum_precision_numfeatures=0;
            score_limited=score(:,1:numFeatures);
            if additional_features
                score_limited=[score_limited other_features];
            end
            neighbour_results=[];
            best_k=-1;
            best_precision=0.0;
            best_model_neighbour=[]
            for numNeighbours=1:4:40 
                cv = cvpartition(y,'KFold',num_folds);
                accum_precision_neighbour=0;
                best_fold_precision=0.0;
                best_model_fold=[];
                for f=1:num_folds
                    idx = training(cv,f); % get partition ith
                    %testIdx = (idx ~= i); % index of test subjects
                    test_fold = score_limited(idx ~= 1, :);
                    train_fold = score_limited(idx == 1, :);
                    y_training = y(idx == 1, :);
                    y_test=y(idx ~=1, :);
                    fprintf('metodo utilizado: %s numero de features: %d y neighbours: %d feet.\n', distance_metric(i),numFeatures,numNeighbours);
                    model=fitcknn(train_fold,y_training,'NumNeighbors',numNeighbours,'Standardize',classification_standardize);
                    [y_predicted, scores_] = predict(model,test_fold);
                    classifier_precision=(length(y_test)-numel(find(y_predicted~=y_test)))/length(y_test);
                    if classifier_precision>best_fold_precision
                        best_model_fold=model;
                        best_fold_precision=classifier_precision;
                    end
                    accum_precision_neighbour= accum_precision_neighbour+classifier_precision;
                end
                mean_precision=accum_precision_neighbour/num_folds;
                if mean_precision>best_precision
                   best_precision=mean_precision;
                   best_k=numNeighbours;
                   best_model_neighbour=best_model_fold;
                end    
                %neighbour_results=[neighbour_results accum_precision_neighbour/num_folds];
            end
            %featurenumber_results=[featurenumber_results;neighbour_results];
            featurenumber_results=[featurenumber_results best_precision];
            best_Ks=[best_Ks best_k];
            best_models_feature{numFeatures-9}=best_model_neighbour;
        end
        %anyadir como primer indice del array multidimnensional
        results_knn=[results_knn; featurenumber_results ];
        results_k= [results_k;best_Ks];
        results_knn_models=[results_knn_models; best_models_feature];
    end
end


function plot_svm_definitive(resultados_svm,roi,data_size)
    methods=["lineal","cuadrático","cúbico","gausiano"];
    X_axis=10:data_size;
    if roi
        X_axis=X_axis+48;
    end    
    for i=1:4
        figure(i)
        plot(X_axis,resultados_svm(i,:));
        title("Evolución de la precisión de SVM con kernel "+methods(i))
        ylabel('Precisión(%)')
        if roi
            xlabel('Número de features utilizadas( 10:213 vóxels + 48 adicionales)')
        else
            xlabel('Número de features utilizadas( 10:213 vóxels)')
        end
    end    
end



function plot_knn_definitive(resultados_knn,resultados_k,roi,data_size)
    distance_metric=["chebychev","correlation","cosine","euclidean","mahalanobis","minkowski"];    
    X_axis=10:data_size;
    if roi
        X_axis=X_axis+48;
    end    
    for i=1:6
        figure(i+4)
        plot(X_axis,resultados_knn(i,:));
        title("Evolución de la precisión de KNN con métrica de distancia "+distance_metric(i))
        ylabel('Precisión(%)')
        if roi
            xlabel('Número de features utilizadas( 10:213 vóxels + 48 adicionales)')
        else
            xlabel('Número de features utilizadas( 10:213 vóxels)')
        end
    end
    
    for i=1:6
        figure(i+10)
        plot(X_axis,resultados_k(i,:));
        title("Número de vecinos(k) con métrica de distancia "+distance_metric(i))
        ylabel('Número de vecinos')
        if roi
            xlabel('Número de features( 10:213 vóxels + 48 adicionales)')
        else
            xlabel('Número de features( 10:213 vóxels)')
        end
    end
    
end

function plot_svm_pca_ttest(roi,data_size)
    methods=["lineal","cuadrático","cúbico","gausiano"];    
    %load pca_nozeros_voxel;
    load pca_nozeros_rois;
    resultados_svm_pca=resultados_svm;
    %load results_no_roi;
    load resultados_rois;
    resultados_svm_ttest=resultados_svm;
    X_axis_ttest=10:data_size;
    X_axis_pca=10:79;
    if roi
        X_axis_ttest=X_axis_ttest+48;
        X_axis_pca=X_axis_pca+48;
    end
    for i=1:4
        figure(i)
        plot(X_axis_ttest,resultados_svm_ttest(i,:));
        title("Evolución de la precisión de SVM con kernel "+methods(i))
        ylabel('Precisión(%)')
        if roi
            xlabel('Número de features utilizadas( 10:213 vóxels + 48 adicionales)')
        else
            xlabel('Número de features utilizadas( 10:213 vóxels)')
        end
        hold on;
        plot(X_axis_pca,resultados_svm_pca(i,:));
        legend({'T-TEST','PCA'},'Location','southeast');
    end
end


function plot_knn_pca_ttest(roi,data_size,vecinos)
    distance_metric=["chebychev","correlation","cosine","euclidean","mahalanobis","minkowski"];    
    %load pca_nozeros_voxel;
    load pca_nozeros_rois;
    resultados_knn_pca=resultados_knn;
    resultados_k_pca=resultados_k;
    %load results_no_roi;
    load resultados_rois;
    resultados_knn_ttest=resultados_knn;
    resultados_k_ttest=resultados_k;
    X_axis_ttest=10:data_size;
    X_axis_pca=10:79;
    if roi
        X_axis_ttest=X_axis_ttest+48;
        X_axis_pca=X_axis_pca+48;
    end
    for i=1:6
        figure(i+4)
        plot(X_axis_ttest,resultados_knn_ttest(i,:));
        title("Evolución de la precisión de KNN con métrica de distancia "+distance_metric(i))
        ylabel('Precisión(%)')
        if roi
            xlabel('Número de features utilizadas( 10:213 vóxels + 48 adicionales)')
        else
            xlabel('Número de features utilizadas( 10:213 vóxels)')
        end
        hold on;
        plot(X_axis_pca,resultados_knn_pca(i,:));
        legend({'T-TEST','PCA'},'Location','southeast');
    end
    if vecinos
        for i=1:6
            figure(i+10)
            plot(X_axis_ttest,resultados_k_ttest(i,:));
            title("Número de vecinos(k) con métrica de distancia "+distance_metric(i))
            ylabel('Número de vecinos')
            if roi
                xlabel('Número de features( 10:213 vóxels + 48 adicionales)')
            else
                xlabel('Número de features( 10:213 vóxels)')
            end
            hold on;
            plot(X_axis_pca,resultados_k_pca(i,:));
        end
    end
end

function [svm_precisions,knn_precisions,k_values]=get_best_results(resultados_svm,resultados_knn,resultados_k)
   [svm_precisions,indexes_svm]= max(resultados_svm,[],2);
   [knn_precisions,indexes_knn]= max(resultados_knn,[],2);
   k_values=[];
   for i=1:6
       k_values=[k_values; resultados_k(i,indexes_knn(i));];
   end    
   
end


function [best_model_SVM,kernel_type,num_features_SVM,best_model_KNN,distance_metric,num_features_KNN]=get_best_model(resultados_svm,models_svm,resultados_knn,models_knn)
   %[best_svm,index_svm]=max(resultados_svm,[],2);
   [best_svm,index_svm]=max(resultados_svm,[],2);
   [max_,i]=max(best_svm);
   a=index_svm(i);
   kernel_type=i;
   num_features_SVM=index_svm(i);
   best_model_SVM=models_svm{i,index_svm(i)};
   
   [best_knn,index_knn]=max(resultados_knn,[],2);
   [max_,i]=max(best_knn);
   distance_metric=i;
   num_features_KNN=index_knn(i);
   best_model_KNN=models_knn{i,index_knn(i)};
   
end


%get weights of best models trained
function [weights_SVM,weights_KNN]=get_weights(best_model_SVM, best_model_KNN)
    %weights_SVM=best_model_SVM.W;
    %weights_KNN=best_model_KNN.Beta;
    weights_KNN=[];
end




