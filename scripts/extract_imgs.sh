#!/bin/bash
for file in *; do
	echo "$file" 
	cd "$file"
	for i in {1..3}; do
		dir=$( ls | head -n1 )
		echo "$dir" 
		cd "$dir"		 
	done
  	img=$( ls )
  	cp "$img" "../../../../$img"
  	cd ../../../../
  	rm -r "$file"
done