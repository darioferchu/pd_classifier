#!/usr/bin/perl

use strict;
use warnings;

my $dir = 'MNI152AffineNormalizedN3NoSkull';

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

# Use a regular expression to ignore files beginning with a period

next if ($file =~ m/^\./);

print "Applying fast to $file\n";

my $input = "MNI152AffineNormalizedN3NoSkull/$file";
my $output = "fastResults/$file";

system( "/usr/local/fsl/bin/fast -o $output $input" );

}

closedir(DIR);
exit 0;