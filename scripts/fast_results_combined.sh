#!/bin/bash
cd fastResultsCombined
for file in *; do
	
	if [[ $file == *"_pve_0"* ]]; then
 		echo "Tomo CSF $file"
 		vol=`$FSLDIR/bin/fslstats "$file" -V | awk '{print $1}'`
		mean=`$FSLDIR/bin/fslstats "$file" -M`
		tissuevol0=`echo "$mean * $vol" | bc -l`
		echo $tissuevol0 >> ../CSF_results_combined.txt
	elif [[ $file == *"_pve_1"* ]]; then
		echo "Tomo GM $file"
		vol=`$FSLDIR/bin/fslstats "$file" -V | awk '{print $1}'`
		mean=`$FSLDIR/bin/fslstats "$file" -M`
		tissuevol0=`echo "$mean * $vol" | bc -l`
		echo $tissuevol0 >> ../GM_results_combined.txt
	elif [[ $file == *"_pve_2"* ]]; then
		echo "Tomo WM $file"
		vol=`$FSLDIR/bin/fslstats "$file" -V | awk '{print $1}'`
		mean=`$FSLDIR/bin/fslstats "$file" -M`
		tissuevol0=`echo "$mean * $vol" | bc -l`
		echo $tissuevol0 >> ../WM_results_combined.txt
	fi
done
cd ..