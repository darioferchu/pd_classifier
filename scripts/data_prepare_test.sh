#!/bin/bash

#unzip PD_control, PD_positives
unzip TEST_POSITIVES.zip
mv PPMI TEST_POSITIVES
unzip TEST_CONTROL.zip
mv PPMI TEST_CONTROL

#Extract images and remove PPMI header
cd TEST_POSITIVES
../scripts/extract_imgs.sh
../scripts/change_names.sh
cd ..

cd TEST_CONTROL
../scripts/extract_imgs.sh
../scripts/change_names.sh
cd ..



