#!/bin/bash

# BrStem_first.vtk -> 1
# L_Accu_first.vtk -> 2
# L_Amyg_first.vtk -> 3
# L_Caud_first.vtk -> 4
# L_Hipp_first.vtk -> 5
# L_Pall_first.vtk -> 6
# L_Puta_first.vtk -> 7
# L_Thal_first.vtk -> 8
# R_Accu_first.vtk -> 9
# R_Amyg_first.vtk -> 10
# R_Caud_first.vtk -> 11
# R_Hipp_first.vtk -> 12
# R_Pall_first.vtk -> 13
# R_Puta_first.vtk -> 14
# R_Thal_first.vtk -> 15

cd firstResults
for folder in *; do
	echo "Carpeta de: $folder"
	cd $folder
	i=1
	for file in *; do
		zona=$( echo "$file" | cut -b -20 )
		if [[ $file == *".vtk"* ]]; then
			echo "	El fichero cogido: $file"
			values=$( python3 ../../scripts/vtkValues.py $file ) 
			# echo $values | cut -d ' ' -f 1
			# echo $values | cut -d ' ' -f 2
			# echo $values | cut -d ' ' -f 3
			file_name1="${i}_1"
			file_name2="${i}_2"
			file_name3="${i}_3"
			echo "	Nombre file: $file_name1"
			echo "	Nombre file: $file_name2"
			echo "	Nombre file: $file_name3"
			echo $values | cut -d ' ' -f 1 >> ../../files/${file_name1}
			echo $values | cut -d ' ' -f 2 >> ../../files/${file_name2}
			echo $values | cut -d ' ' -f 3 >> ../../files/${file_name3}
			((i=i+1))
		fi
	done	
	cd ..
done
cd ..
