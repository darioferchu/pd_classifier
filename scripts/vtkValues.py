import vtk
import sys


#print(sys.argv[1])
reader = vtk.vtkPolyDataReader()
reader.SetFileName(sys.argv[1])
# reader.ReadAllScalarsOn()
# reader.ReadAllVectorsOn()
# reader.ReadAllTensorsOn()
reader.Update()
vtkdata = reader.GetOutput()

Mass = vtk.vtkMassProperties()
Mass.SetInputData(vtkdata)
Mass.Update() 

print(Mass.GetVolume()) 
print(Mass.GetSurfaceArea())
#Compute and return the normalized shape index. This characterizes the deviation of the shape of an object from a sphere. A sphere's NSI is one. This number is always >= 1.0.
print(Mass.GetNormalizedShapeIndex())
