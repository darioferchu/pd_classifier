#!/usr/bin/perl

use strict;
use warnings;

my $dir = 'ReorientedScreeningTest';

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

# Use a regular expression to ignore files beginning with a period
next if ($file =~ m/^\./);

print "$file\n";

my $input = "ReorientedScreeningTest/$file";
my $output = "MNI152AffineNormalizedScreeningTest/$file";
my $mat = "MNI152AffineNormalizedScreeningTest/$file".".mat";

system( "/usr/local/fsl/bin/flirt -in $input -ref /usr/local/fsl/data/standard/MNI152_T1_1mm.nii.gz -out $output -omat $mat -bins 256 -cost corratio -searchrx 0 0 -searchry 0 0 -searchrz 0 0 -dof 12  -interp trilinear" );

}

closedir(DIR);
exit 0;
