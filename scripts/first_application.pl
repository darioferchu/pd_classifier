#!/usr/bin/perl

use strict;
use warnings;

my $dir = 'ReorientedScreeningTest';

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

# Use a regular expression to ignore files beginning with a period

next if ($file =~ m/^\./);

print "Applying first to $file\n";

my $folder=substr($file, 0, index($file, '_'));
system( "mkdir firstResultsTest/$folder" );
my $input = "ReorientedScreeningTest/$file";
my $output = "firstResultsTest/$folder/$file";

system( "/usr/local/fsl/bin/run_first_all -d -i $input -o $output" );

}

closedir(DIR);
exit 0;