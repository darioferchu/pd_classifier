#!/usr/bin/perl

use strict;
use warnings;

my $dir = 'MNI152AffineNormalizedScreeningTest';

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

# print "$file\n";
# print "primera\n";
# Use a regular expression to ignore files beginning with a period
next if ($file =~ m/^\./);
next if ($file =~ /\.gz$/);
next if ($file =~ /\.mat$/);

print "$file\n";

my $input = "MNI152AffineNormalizedScreeningTest/$file";
my $output = "MNI152AffineNormalizedN3Test/$file";

#system( "./N3BiasFieldCorrection 3 $input $output 2" );
system( "../install/bin/N3BiasFieldCorrection 3 $input $output 2" );
}

closedir(DIR);
exit 0;
