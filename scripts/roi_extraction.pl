#!/usr/bin/perl

use strict;
use warnings;

my $dir = 'final_data_40';

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

# Use a regular expression to ignore files beginning with a period

next if ($file =~ m/^\./);

print "$file\n";

my $input = "final_data_40/$file";
my $output = "final_data_ROI/$file";

system( "/usr/local/fsl/bin/fslreorient2std $input $output" );

}
closedir(DIR);
exit 0;