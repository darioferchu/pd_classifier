#!/bin/bash

#unzip PD_control, PD_positives
unzip PD_positives.zip
mv PPMI PD_positives
unzip PD_control.zip
mv PPMI PD_control

#Extract images and remove PPMI header
cd PD_positives
../scripts/extract_imgs.sh
../scripts/change_names.sh
cd ..

cd PD_control
../scripts/extract_imgs.sh
../scripts/change_names.sh
cd ..



