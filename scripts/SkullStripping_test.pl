#!/usr/bin/perl

use strict;
use warnings;

my $dir = 'MNI152AffineNormalizedN3Test';

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

# Use a regular expression to ignore files beginning with a period

next if ($file =~ m/^\./);

print "Stripping $file\n";

my $input = "MNI152AffineNormalizedN3Test/$file";
my $output = "MNI152AffineNormalizedN3NoSkullTest/$file";

system( "/usr/local/fsl/bin/bet $input $output" );

}

closedir(DIR);
exit 0;