#!/usr/bin/perl

use strict;
use warnings;

my $dir=$ARGV[0];

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

# Use a regular expression to ignore files beginning with a period

next if ($file =~ m/^\./);

print "$file\n";

my $input = "$dir/$file";
my $output = "ReorientedScreeningTest/$file";

system( "/usr/local/fsl/bin/fslreorient2std $input $output" );

}
closedir(DIR);
exit 0;
